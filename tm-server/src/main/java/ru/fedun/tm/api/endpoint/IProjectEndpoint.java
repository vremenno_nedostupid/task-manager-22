package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.entity.Session;

import java.util.List;

public interface IProjectEndpoint {

    void createProject(@NotNull Session session, @NotNull String name, @NotNull String description) throws Exception;

    void clearProjects(@NotNull Session session) throws Exception;

    @NotNull
    List<Project> showAllProjects(@NotNull Session session) throws Exception;

    @NotNull
    Project showProjectById(@NotNull Session session, @NotNull String id) throws Exception;

    @NotNull
    Project showProjectByIndex(@NotNull final Session session, @NotNull final Integer index) throws Exception;

    @NotNull
    Project showProjectByName(@NotNull Session session, @NotNull String name) throws Exception;

    @NotNull
    Project updateProjectById(
            @NotNull Session session,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    Project updateProjectByIndex(
            @NotNull Session session,
            @NotNull Integer index,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    Project removeProjectById(@NotNull Session session, @NotNull String id) throws Exception;

    @NotNull
    Project removeProjectByIndex(@NotNull Session session, @NotNull Integer index) throws Exception;

    @NotNull
    Project removeProjectByName(@NotNull Session session, @NotNull String name) throws Exception;

}
