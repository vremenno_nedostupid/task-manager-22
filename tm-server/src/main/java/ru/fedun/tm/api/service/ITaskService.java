package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void create(@NotNull String userId, @NotNull String title);

    @NotNull
    List<Task> findAll(@NotNull final String userId);

    void create(@NotNull String userId, @NotNull String title, @NotNull String description);

    void clear(@NotNull String userId);

    @NotNull
    Task getOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task getOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task getOneByTitle(@NotNull String userId, @NotNull String title);

    @NotNull
    Task updateById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull String title,
            @NotNull String description
    );

    @NotNull
    Task updateByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @NotNull String title,
            @NotNull String description
    );

    @NotNull
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task removeOneByTitle(@NotNull String userId, @NotNull String title);

}
