package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;

public interface IAdminEndpoint {

    @NotNull
    User createUserWithEmail(
            @NotNull String login,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull String password,
            @NotNull String email
    ) throws Exception;

    @NotNull
    User createUserWithRole(
            @NotNull Session session,
            @NotNull String login,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull String password,
            @NotNull Role role
    ) throws Exception;

    @NotNull
    User lockUserByLogin(@NotNull Session session, @NotNull String login) throws Exception;

    @NotNull
    User unlockUserByLogin(@NotNull Session session, @NotNull String login) throws Exception;

    void removeUserByLogin(@NotNull Session session, @NotNull String login) throws Exception;

}
