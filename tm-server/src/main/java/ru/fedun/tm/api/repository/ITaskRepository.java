package ru.fedun.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    @NotNull
    List<Task> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    void add(@NotNull String userId, @NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    @NotNull
    Task findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task findOneByTitle(@NotNull String userId, @NotNull String title);

    @NotNull
    Task removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task removeOneByTitle(@NotNull String userId, @NotNull String title);

}
