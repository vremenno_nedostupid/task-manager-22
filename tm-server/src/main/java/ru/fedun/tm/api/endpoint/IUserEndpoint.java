package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.entity.User;

public interface IUserEndpoint {

    @NotNull
    User updateUserPassword(
            @NotNull Session session,
            @NotNull String password,
            @NotNull String newPassword
    ) throws Exception;

    @NotNull
    User updateUserEmail(@NotNull Session session, @NotNull String newEmail) throws Exception;

    @NotNull
    User showUserProfile(@NotNull Session session) throws Exception;


}
