package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.dto.Result;
import ru.fedun.tm.entity.Session;

public interface ISessionEndpoint {

    @Nullable
    Session openSession(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    Result closeSession(@NotNull Session session) throws Exception;

    @NotNull
    Result closeAllSessionsForUser(@NotNull Session session) throws Exception;

}
