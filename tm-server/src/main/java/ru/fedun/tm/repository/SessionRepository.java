package ru.fedun.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.ISessionRepository;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.exception.unknown.UnknownIdException;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void add(@NotNull String userId, @NotNull Session session) {
        session.setUserId(userId);
        entities.add(session);
    }

    @Override
    public void remove(@NotNull String userId, @NotNull Session session) {
        if (!userId.equals(session.getUserId())) return;
        entities.remove(session);
    }

    @Override
    public void clear(@NotNull String userId) {
        entities.remove(findAllSessions(userId));
    }

    @NotNull
    @Override
    public List<Session> findAllSessions(@NotNull String userId) {
        @NotNull final List<Session> result = new ArrayList<>();
        for (@NotNull final Session session : entities) {
            if (userId.equals(session.getUserId())) result.add(session);
        }
        return result;
    }

    @NotNull
    @Override
    public Session findByUserId(@NotNull String userId) {
        for (@NotNull final Session session : entities) {
            if(userId.equals(session.getUserId())) return session;
        }
        throw new UnknownIdException();
    }

    @NotNull
    @Override
    public Session findById(@NotNull String id) {
        for (@NotNull final Session session : entities) {
            if(id.equals(session.getId())) return session;
        }
        throw new UnknownIdException();
    }

    @NotNull
    @Override
    public Session removeByUserId(@NotNull String userId) {
        @NotNull final Session session = findByUserId(userId);
        remove(userId, session);
        return session;
    }

    @Override
    public boolean contains(@NotNull String id) {
        @NotNull final Session session = findById(id);
        return findAll().contains(session);
    }

}
