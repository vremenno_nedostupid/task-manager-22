package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IRepository;
import ru.fedun.tm.api.service.IService;
import ru.fedun.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    private final IRepository<E> entityRepository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.entityRepository = repository;
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return entityRepository.findAll();
    }

    @Override
    public void load(@NotNull final List<E> es) {
        entityRepository.load(es);
    }

    @Override
    public void load(@NotNull final E... es) {
        entityRepository.load(es);
    }

}
