package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.api.service.ISessionService;
import ru.fedun.tm.endpoint.Session;

public final class SessionService implements ISessionService {

    private Session currentSession;

    @NotNull
    @Override
    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(@NotNull final Session currentSession) {
        this.currentSession = currentSession;
    }

    @Override
    public void clearCurrentSession() {
        currentSession = null;
    }

}
