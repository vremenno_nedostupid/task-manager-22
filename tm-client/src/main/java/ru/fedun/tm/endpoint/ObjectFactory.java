
package ru.fedun.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.fedun.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.fedun.ru/", "Exception");
    private final static QName _ShowUserProfile_QNAME = new QName("http://endpoint.tm.fedun.ru/", "showUserProfile");
    private final static QName _ShowUserProfileResponse_QNAME = new QName("http://endpoint.tm.fedun.ru/", "showUserProfileResponse");
    private final static QName _UpdateUserEmail_QNAME = new QName("http://endpoint.tm.fedun.ru/", "updateUserEmail");
    private final static QName _UpdateUserEmailResponse_QNAME = new QName("http://endpoint.tm.fedun.ru/", "updateUserEmailResponse");
    private final static QName _UpdateUserPassword_QNAME = new QName("http://endpoint.tm.fedun.ru/", "updateUserPassword");
    private final static QName _UpdateUserPasswordResponse_QNAME = new QName("http://endpoint.tm.fedun.ru/", "updateUserPasswordResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.fedun.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link ShowUserProfile }
     * 
     */
    public ShowUserProfile createShowUserProfile() {
        return new ShowUserProfile();
    }

    /**
     * Create an instance of {@link ShowUserProfileResponse }
     * 
     */
    public ShowUserProfileResponse createShowUserProfileResponse() {
        return new ShowUserProfileResponse();
    }

    /**
     * Create an instance of {@link UpdateUserEmail }
     * 
     */
    public UpdateUserEmail createUpdateUserEmail() {
        return new UpdateUserEmail();
    }

    /**
     * Create an instance of {@link UpdateUserEmailResponse }
     * 
     */
    public UpdateUserEmailResponse createUpdateUserEmailResponse() {
        return new UpdateUserEmailResponse();
    }

    /**
     * Create an instance of {@link UpdateUserPassword }
     * 
     */
    public UpdateUserPassword createUpdateUserPassword() {
        return new UpdateUserPassword();
    }

    /**
     * Create an instance of {@link UpdateUserPasswordResponse }
     * 
     */
    public UpdateUserPasswordResponse createUpdateUserPasswordResponse() {
        return new UpdateUserPasswordResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowUserProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "showUserProfile")
    public JAXBElement<ShowUserProfile> createShowUserProfile(ShowUserProfile value) {
        return new JAXBElement<ShowUserProfile>(_ShowUserProfile_QNAME, ShowUserProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShowUserProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "showUserProfileResponse")
    public JAXBElement<ShowUserProfileResponse> createShowUserProfileResponse(ShowUserProfileResponse value) {
        return new JAXBElement<ShowUserProfileResponse>(_ShowUserProfileResponse_QNAME, ShowUserProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "updateUserEmail")
    public JAXBElement<UpdateUserEmail> createUpdateUserEmail(UpdateUserEmail value) {
        return new JAXBElement<UpdateUserEmail>(_UpdateUserEmail_QNAME, UpdateUserEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "updateUserEmailResponse")
    public JAXBElement<UpdateUserEmailResponse> createUpdateUserEmailResponse(UpdateUserEmailResponse value) {
        return new JAXBElement<UpdateUserEmailResponse>(_UpdateUserEmailResponse_QNAME, UpdateUserEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "updateUserPassword")
    public JAXBElement<UpdateUserPassword> createUpdateUserPassword(UpdateUserPassword value) {
        return new JAXBElement<UpdateUserPassword>(_UpdateUserPassword_QNAME, UpdateUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.fedun.ru/", name = "updateUserPasswordResponse")
    public JAXBElement<UpdateUserPasswordResponse> createUpdateUserPasswordResponse(UpdateUserPasswordResponse value) {
        return new JAXBElement<UpdateUserPasswordResponse>(_UpdateUserPasswordResponse_QNAME, UpdateUserPasswordResponse.class, null, value);
    }

}
