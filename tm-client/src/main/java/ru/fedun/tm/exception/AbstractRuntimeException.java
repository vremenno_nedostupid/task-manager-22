package ru.fedun.tm.exception;

import org.jetbrains.annotations.NotNull;

public class AbstractRuntimeException extends RuntimeException {

    public AbstractRuntimeException(@NotNull final String message) {
        super(message);
    }

}
