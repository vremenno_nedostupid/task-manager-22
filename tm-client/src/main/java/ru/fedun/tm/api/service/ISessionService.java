package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.endpoint.Session;

public interface ISessionService {

    @NotNull
    Session getCurrentSession();

    void setCurrentSession(@NotNull Session currentSession);

    void clearCurrentSession();

}

