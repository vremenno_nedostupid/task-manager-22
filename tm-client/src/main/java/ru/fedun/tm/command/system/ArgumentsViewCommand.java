package ru.fedun.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

public final class ArgumentsViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program arguments.";
    }

    @Override
    public void execute() {
        @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (@NotNull final AbstractCommand command : commands) {
            if (command.arg() == null) continue;
            System.out.println(command.arg());
        }
    }

}
