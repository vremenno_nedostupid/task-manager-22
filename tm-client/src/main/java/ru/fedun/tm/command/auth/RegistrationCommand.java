package ru.fedun.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.util.TerminalUtil;

public final class RegistrationCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @NotNull
    @Override
    public String description() {
        return "Registry in task manager.";
    }

    @Override
    public void execute() throws Exception{
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER FIRST NAME:]");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME:]");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL:]");
        @NotNull final String email = TerminalUtil.nextLine();
        serviceLocator.getAdminEndpoint().createUserWithEmail(login, password, firstName, lastName, email);
        System.out.println("[OK]");
        System.out.println();
    }

}
