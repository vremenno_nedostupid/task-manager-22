package ru.fedun.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Alexander Fedun");
        System.out.println("sasha171998@gmail.com");
        System.out.println();
    }
}
