package ru.fedun.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;

public final class LogoutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from task manager.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        @Nullable final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getSessionEndpoint().closeSession(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
