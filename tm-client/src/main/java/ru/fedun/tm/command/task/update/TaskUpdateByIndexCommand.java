package ru.fedun.tm.command.task.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-update-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextInt();
        serviceLocator.getTaskEndpoint().showTaskByIndex(session, index);
        System.out.println("ENTER NEW TITLE:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().updateTaskByIndex(session, index, name, description);
        System.out.println("[OK]");
    }

}
